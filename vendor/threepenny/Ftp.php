<?php

namespace Threepenny;

class Ftp
{
    private $message;
    private $connection = null;
    private $rootServer;
    private $caption;

    public function getMessage()
    {
        return $this->message;
    }

    public function mergeMessage($message) {
        return array_merge($message, $this->message);
    }

    function __construct($caption, $server, $user, $password, $rootServer)
    {
        if ($this->connect($caption, $server, $user, $password)) {
             $this->rootServer = $rootServer;
             $this->caption = $caption;
            // maak de myap folder indien nodig
            if (!@ftp_chdir($this->connection, $this->rootServer)) {
                ftp_mkdir($this->connection, $this->rootServer);
                // ftp_chdir($this->connection, $folder);
            }
        }
    }

    function __destruct()
    {
        if ($this->connection !== null) {
            ftp_close($this->connection);
        }
    }

    public function connect($caption, $server, $user, $password)
    {
        $success = false;
        if ($this->connection !== null) {
            $this->message[] = 'Connectie is al gemaakt.';
            $success = true;
        } else {
            try {
                $this->connection = ftp_connect($server);
                $login = \ftp_login($this->connection, $user, $password);
                // turn passive mode on
                ftp_pasv($this->connection, true);
                $this->message[] = "Connectie met {$caption} is gemaakt.";
                $success = true;
            } catch (\PDOException $e) {
                $this->message[] = $e->getMessage();
            }
        }
        return $success;
    }

    public function ftpBackup($fileName)
    {
        $succes = false;
        $fullName = DOCUMENT_ROOT . '/' . $fileName;
        $path = pathinfo($fileName, PATHINFO_DIRNAME);
        $fileName = pathinfo($fileName, PATHINFO_FILENAME) . '.' . pathinfo($fileName, PATHINFO_EXTENSION);
        $parts = explode('/', ltrim($path, '/'));

        // bestaat het bestand al in de publicatie
        // info: https://stackoverflow.com/questions/4852767/how-can-i-check-if-a-file-exists-on-a-remote-server-using-php
        $file_size = ftp_size($this->connection, $this->rootServer . '/' . $path . '/' . $fileName);
        if ($file_size != -1) {
            // vergelijk de grootte om te zien als bestand gewijzigd is, kan beter, maar...
            if (filesize($fullName) == $file_size) {
                $this->message[] = "$fullName bestand heeft zelfde grootte, waarschijnlijk niet gewijzigd.";
            }
            else {
                // File exists
                ftp_chdir($this->connection, $this->rootServer . '/' . MYAP_FOLDER);
                if (!@ftp_chdir($this->connection, HISTORY_FOLDER)) {
                    ftp_mkdir($this->connection, HISTORY_FOLDER);
                    ftp_chdir($this->connection, HISTORY_FOLDER);
                }
                // make een backup
                foreach ($parts as $part) {
                    // echo $part;
                    if (!@ftp_chdir($this->connection, $part)) {
                        ftp_mkdir($this->connection, $part);
                        ftp_chdir($this->connection, $part);
                    }
                }
                $day = date("d");
                $time = date("H.i.s");
                if (ftp_rename($this->connection, $this->rootServer . '/' . $path . '/' . $fileName,
                    "{$day}_{$time}__{$fileName}")) {
                    $this->message[] = "Backup gemaakt van het bestand $fileName op {$this->caption}.";
                    $succes = true;
                } else {
                    $this->message[] = "Er is iets fout gelopen met het maken van een backup van $fileName op {$this->caption}.";
                }
            }
        }
        return $succes;
    }

    public function ftpDelete($fileName)
    {
        $success = false;
        $fullName = MYAP_PUBLIC_FOLDER . '/' . $fileName;
        // bestaat het bestand al in de publicatie
        // info: https://stackoverflow.com/questions/4852767/how-can-i-check-if-a-file-exists-on-a-remote-server-using-php
        $file_size = ftp_size($this->connection, $fullName);
        if ($file_size != -1) {
           if (ftp_delete($this->connection, $fullName)) {
                $this->message[] = "Het bestand $fileName kon niet worden verwijderd op {$this->caption}.";
                $success = true;
            } else {
                $this->message[] = "Het bestand $fileName bestaat niet en kon niet worden verwijderd.";
            }
        } else {
            $this->message[] = "Het bestand $fileName bestaat niet op {$this->caption} en is dus niet verwijderd.";

        }
        return $success;
    }

    public function ftpFileExists($fileName) {
        $fullName = MYAP_PUBLIC_FOLDER . '/' . $fileName;
        // bestaat het bestand al in de publicatie
        // info: https://stackoverflow.com/questions/4852767/how-can-i-check-if-a-file-exists-on-a-remote-server-using-php
        $file_size = ftp_size($this->connection, $fullName);
        if ($file_size == -1) {
                $this->message[] = "Het bestand $fileName is niet gepubliceerd op {$this->caption}.";
                $success = false;
            } else {
                $this->message[] = "Het bestand $fileName is gepubliceerd op {$this->caption}.";
                $success = true;
            }
        return $success;
    }

    public function ftpPut($fileName)
    {
        $succes = false;
        $fullName = DOCUMENT_ROOT . '/' . $fileName;
        $path = pathinfo($fileName, PATHINFO_DIRNAME);
        $fileName = pathinfo($fileName, PATHINFO_FILENAME) . '.' . pathinfo($fileName, PATHINFO_EXTENSION);
        $parts = explode('/', ltrim($path, '/'));

        ftp_chdir($this->connection, $this->rootServer);
        // put file on the server
        foreach ($parts as $part) {
            // echo $part;
            if (!@ftp_chdir($this->connection, $part)) {
                ftp_mkdir($this->connection, $part);
                ftp_chdir($this->connection, $part);
            }
        }
        ftp_pasv($this->connection, true);
        if (ftp_put($this->connection, $fileName, $fullName, FTP_BINARY)) {
            $this->message[] = "Het bestand $fileName is verzonden naar {$this->caption}.";
            $succes = true;
        } else {
            $this->message[] = "Kan het bestand $fileName niet verzenden naar {$this->caption}.";
        }
        return $succes;
    }

    public function ftpPutAll($fileNames) {
        foreach ($fileNames as $fileName) {
            $this->ftpPut($fileName);
        }
    }
}

