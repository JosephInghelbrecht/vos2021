<!--  CreatingOne View for Procedure entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Procedure/CreatingOne.php/CreatingOne.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
	<form class="detail" id="form" action="/Procedure/createOne" method="post">
		<header>
			<h2 class="banner">Creating One Procedure</h2>
			<nav class="command-panel">
				<button type="submit" value="createOne" name="createOne" class="tile">
					<span class="icon-floppy-disk"></span>
					<span class="screen-reader-text">Create One</span>
				</button>
				<a href="/Procedure/Index" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="Procedure-Code">Code</label>
				<input id="Procedure-Code" name="Procedure-Code" class="text" style="width: 12.5%;" type="text" value="" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="Procedure-Name">Naam</label>
				<input id="Procedure-Name" name="Procedure-Name" class="text" style="width: 80%;" type="text" value="" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="Procedure-Description">Omschrijving</label>
				<input id="Procedure-Description" name="Procedure-Description" class="text" style="width: 80%;" type="text" value=""   />
			</div>
			<div class="field">
				<label for="Procedure-RoleId">Rol</label>
				<select id="Procedure-RoleId" name="Procedure-RoleId" required >
				<?php
				if (count($model['RoleList']) > 0)
				{
					$i = 1;
					foreach ($model['RoleList'] as $item)
					{
					?>
					<option value="<?php echo $item['Id'];?>" <?php echo ($i++ == 1 ? ' selected' : '');?>>
				<?php echo $item['Code'] . ' ' . $item['Name'];?>
					<?php
					}
				}
				?>
				</select>
					<span>*</span>
			</div>
			<div class="field">
				<label for="Procedure-UpdatedOnDate">Laatst gewijzigd op</label>
				<input id="Procedure-UpdatedOnDate" value="<?php echo date('Y-m-d');?>" type="date" name="Procedure-UpdatedOnDate"   required />
				<label for="Procedure-UpdatedOnTime">om</label>
				<input id="Procedure-UpdatedOnTime"  value="<?php echo date('H:i:s');?>" type="time" name="Procedure-UpdatedOnTime"   required />
					<span>*</span>
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</form>
	<?php include('ReadingAll.php'); ?>
<?php include('Views/Vos/PageFooter.php');?>
</main>
