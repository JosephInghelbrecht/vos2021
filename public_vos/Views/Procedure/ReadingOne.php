<!--  ReadingOne View for Procedure entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Procedure/ReadingOne.php/ReadingOne.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
	<section class="detail" id="form" action="/Procedure/createOne" method="post">
		<header>
			<h2 class="banner">Reading One Procedure</h2>
			<nav class="command-panel">
				<a href="/Procedure/UpdatingOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-pencil"></span>
					<span class="screen-reader-text">Updating One</span>
				</a>
				<a href="/Procedure/CreatingOne" class="tile">
					<span class="icon-plus"></span>
					<span class="screen-reader-text">Creating One</span>
				</a>
				<a href="/Procedure/DeleteOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-bin"></span>
					<span class="screen-reader-text">Delete One</span>
				</a>
				<a href="/Procedure/Index" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="Procedure-Code">Code</label>
				<input id="Procedure-Code" name="Procedure-Code" class="text" style="width: 12.5%;" type="text" value="<?php echo $model['row']['Code'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Procedure-Name">Naam</label>
				<input id="Procedure-Name" name="Procedure-Name" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Name'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Procedure-Description">Omschrijving</label>
				<input id="Procedure-Description" name="Procedure-Description" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Description'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Procedure-RoleId">Rol</label>
				<select id="Procedure-RoleId" name="Procedure-RoleId"  disabled>
				<?php
				if (count($model['RoleList']) > 0)
				{
					$i = 1;
					foreach ($model['RoleList'] as $item)
					{
					?>
					<option value="<?php echo $item['Id'];?>" <?php echo ($model['row']['RoleId']  == $item['Id'] ? ' selected' : '');?>>
				<?php echo $item['Code'] . ' ' . $item['Name'];?>
					<?php
					}
				}
				?>
				</select>
			</div>
			<div class="field">
				<label for="Procedure-UpdatedOnDate">Laatst gewijzigd op</label>
				<input id="Procedure-UpdatedOnDate" value="<?php echo date('Y-m-d', strtotime($model['row']['UpdatedOn']));?>" type="date" name="Procedure-UpdatedOnDate"  disabled  />
				<label for="Procedure-UpdatedOnTime">om</label>
				<input id="Procedure-UpdatedOnTime"  value="<?php echo date('H:i:s', strtotime($model['row']['UpdatedOn']));?>" type="time" name="Procedure-UpdatedOnTime"  disabled  />
			</div>
			<div class="field">
				<label for="Procedure-Id">NA</label>
				<input id="Procedure-Id" name="Procedure-Id" class="integer" style="width: 6em;" type="text" value="<?php echo $model['row']['Id'];?>"  disabled />
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</section>
	<?php include('ReadingAll.php'); ?>
</main>
<?php include('Views/Vos/PageFooter.php');?>
