<!--  Index view for Procedure entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Views/Procedure/Index.php/Index.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
	<section class="detail">
	<header>
		<h2 class="banner">Index Procedure</h2>
		<nav class="command-panel">
			<a href="/Procedure/CreatingOne" class="tile">
				<span class="icon-plus"></span>
				<span class="screen-reader-text">Creating One</span>
			</a>
		</nav>
	</header>
	<fieldset>
	</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</section>
	<?php include('ReadingAll.php'); ?>
</main>
<?php include('Views/Vos/PageFooter.php');?>
