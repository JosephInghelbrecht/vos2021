<!--  Page header for Vos App
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Vos/PageFooter.php
-->
<header class="front">
	<nav class="control-panel">
		<a href="/Vos/index" class="tile"><span class="icon-menu"></span><span class="screen-reader-text">Home index</span></a>
		<?php
		if (isset($model['user-name'])) { ?>
			<p>Welkom <?php echo $model['user-name']; ?></p>
			<a class="tile" href="/Vos/logout/"><span class="icon-exit"></span><span class="screen-reader-text">afmelden</span></a>
		<?php } ?>
	</nav>
	<h1>Vos</h1>
</header>
