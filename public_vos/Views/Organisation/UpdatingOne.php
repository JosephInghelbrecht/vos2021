<!--  UpdatingOne View for Organisation entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Organisation/UpdatingOne.php/UpdatingOne.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
	<form class="detail" id="form" action="/Organisation/UpdateOne" method="post">
		<header>
			<h2 class="banner">Updating One Organisation</h2>
			<nav class="command-panel">
				<button type="submit" value="updateOne" name="updateOne" class="tile">
					<span class="icon-floppy-disk"></span>
					<span class="screen-reader-text">Update One</span>
				</button>
				<a href="/Organisation/ReadingOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="Organisation-Name">Naam</label>
				<input id="Organisation-Name" name="Organisation-Name" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Name'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="Organisation-Street">Adres</label>
				<input id="Organisation-Street" name="Organisation-Street" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Street'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="Organisation-PostalCode">Postcode</label>
				<input id="Organisation-PostalCode" name="Organisation-PostalCode" class="text" style="width: 5%;" type="text" value="<?php echo $model['row']['PostalCode'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="Organisation-City">Woonplaats</label>
				<input id="Organisation-City" name="Organisation-City" class="text" style="width: 20%;" type="text" value="<?php echo $model['row']['City'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="Organisation-Latitude">Breedtegraad</label>
				<input id="Organisation-Latitude" name="Organisation-Latitude" class="decimal" type="text" value="<?php echo $model['row']['Latitude'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="Organisation-Longitude">Lengtegraad</label>
				<input id="Organisation-Longitude" name="Organisation-Longitude" class="decimal" type="text" value="<?php echo $model['row']['Longitude'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="Organisation-NameManagement">Naam directie</label>
				<input id="Organisation-NameManagement" name="Organisation-NameManagement" class="text" style="width: 20%;" type="text" value="<?php echo $model['row']['NameManagement'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="Organisation-TelManagement">Telefoon Directie</label>
				<input id="Organisation-TelManagement" name="Organisation-TelManagement" class="text" style="width: 6.25%;" type="text" value="<?php echo $model['row']['TelManagement'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<input id="Organisation-Id" name="Organisation-Id" style="width: 6em;" type="hidden" value="<?php echo $model['row']['Id'];?>" required  />
			</div>
			<div class="field">
				<label for="Organisation-UpdatedOnDate">Laatst gewijzigd op</label>
				<input id="Organisation-UpdatedOnDate" value="<?php echo date('Y-m-d', strtotime($model['row']['UpdatedOn']));?>" type="date" name="Organisation-UpdatedOnDate"   required />
				<label for="Organisation-UpdatedOnTime">om</label>
				<input id="Organisation-UpdatedOnTime"  value="<?php echo date('H:i:s', strtotime($model['row']['UpdatedOn']));?>" type="time" name="Organisation-UpdatedOnTime"   required />
					<span>*</span>
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</form>
	<?php include('ReadingAll.php'); ?>
</main>
<?php include('Views/Vos/PageFooter.php');?>
