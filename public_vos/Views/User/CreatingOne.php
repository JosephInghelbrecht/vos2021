<!--  CreatingOne View for User entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/User/CreatingOne.php/CreatingOne.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
	<form class="detail" id="form" action="/User/createOne" method="post">
		<header>
			<h2 class="banner">Creating One User</h2>
			<nav class="command-panel">
				<button type="submit" value="createOne" name="createOne" class="tile">
					<span class="icon-floppy-disk"></span>
					<span class="screen-reader-text">Create One</span>
				</button>
				<a href="/User/Index" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="User-Name">Naam</label>
				<input id="User-Name" name="User-Name" class="text" style="width: 12.5%;" type="text" value="" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="User-Password">Hash</label>
				<input id="User-Password" name="User-Password" style="width: 80%;" type="password" value=""   />
			</div>
			<div class="field">
				<label for="User-RoleId">Rol</label>
				<select id="User-RoleId" name="User-RoleId" required >
				<?php
				if (count($model['RoleList']) > 0)
				{
					$i = 1;
					foreach ($model['RoleList'] as $item)
					{
					?>
					<option value="<?php echo $item['Id'];?>" <?php echo ($i++ == 1 ? ' selected' : '');?>>
				<?php echo $item['Code'] . ' ' . $item['Name'];?>
					<?php
					}
				}
				?>
				</select>
					<span>*</span>
			</div>
			<div class="field">
				<label for="User-Email">Email</label>
				<input id="User-Email" name="User-Email" style="width: 80%;" type="email" value="" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="User-PhoneWork">Telefoon werk</label>
				<input id="User-PhoneWork" name="User-PhoneWork" class="text" style="width: 6.25%;" type="text" value=""   />
			</div>
			<div class="field">
				<label for="User-FunctionId">Functie</label>
				<select id="User-FunctionId" name="User-FunctionId" required >
				<?php
				if (count($model['FunctionList']) > 0)
				{
					$i = 1;
					foreach ($model['FunctionList'] as $item)
					{
					?>
					<option value="<?php echo $item['Id'];?>" <?php echo ($i++ == 1 ? ' selected' : '');?>>
				<?php echo $item['Name'];?>
					<?php
					}
				}
				?>
				</select>
					<span>*</span>
			</div>
			<div class="field">
				<label for="User-OrganisationId">Organisatie</label>
				<select id="User-OrganisationId" name="User-OrganisationId"  >
				<?php
				if (count($model['OrganisationList']) > 0)
				{
					$i = 1;
					foreach ($model['OrganisationList'] as $item)
					{
					?>
					<option value="<?php echo $item['Id'];?>" <?php echo ($i++ == 1 ? ' selected' : '');?>>
				<?php echo $item['Name'];?>
					<?php
					}
				}
				?>
				</select>
			</div>
			<div class="field">
				<label for="User-PersonId">Persoon</label>
				<select id="User-PersonId" name="User-PersonId" required >
				<?php
				if (count($model['PersonList']) > 0)
				{
					$i = 1;
					foreach ($model['PersonList'] as $item)
					{
					?>
					<option value="<?php echo $item['Id'];?>" <?php echo ($i++ == 1 ? ' selected' : '');?>>
				<?php echo $item['FirstName'] . ' ' . $item['Lastname'];?>
					<?php
					}
				}
				?>
				</select>
					<span>*</span>
			</div>
			<div class="field">
				<label for="User-CreatedOnDate">Laatst gewijzigd op</label>
				<input id="User-CreatedOnDate" value="<?php echo date('Y-m-d');?>" type="date" name="User-CreatedOnDate"   required />
				<label for="User-CreatedOnTime">om</label>
				<input id="User-CreatedOnTime"  value="<?php echo date('H:i:s');?>" type="time" name="User-CreatedOnTime"   required />
					<span>*</span>
			</div>
			<div class="field">
				<label for="User-UpdatedOnDate">Laatst gewijzigd op</label>
				<input id="User-UpdatedOnDate" value="<?php echo date('Y-m-d');?>" type="date" name="User-UpdatedOnDate"   required />
				<label for="User-UpdatedOnTime">om</label>
				<input id="User-UpdatedOnTime"  value="<?php echo date('H:i:s');?>" type="time" name="User-UpdatedOnTime"   required />
					<span>*</span>
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</form>
	<?php include('ReadingAll.php'); ?>
<?php include('Views/Vos/PageFooter.php');?>
</main>
