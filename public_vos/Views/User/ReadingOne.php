<!--  ReadingOne View for User entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/User/ReadingOne.php/ReadingOne.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
	<section class="detail" id="form" action="/User/createOne" method="post">
		<header>
			<h2 class="banner">Reading One User</h2>
			<nav class="command-panel">
				<a href="/User/UpdatingOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-pencil"></span>
					<span class="screen-reader-text">Updating One</span>
				</a>
				<a href="/User/CreatingOne" class="tile">
					<span class="icon-plus"></span>
					<span class="screen-reader-text">Creating One</span>
				</a>
				<a href="/User/DeleteOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-bin"></span>
					<span class="screen-reader-text">Delete One</span>
				</a>
				<a href="/User/Index" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="User-Name">Naam</label>
				<input id="User-Name" name="User-Name" class="text" style="width: 12.5%;" type="text" value="<?php echo $model['row']['Name'];?>"  disabled />
			</div>
			<div class="field">
				<label for="User-Password">Hash</label>
				<input id="User-Password" name="User-Password" style="width: 80%;" type="password" value="<?php echo $model['row']['Password'];?>"  disabled />
			</div>
			<div class="field">
				<label for="User-RoleId">Rol</label>
				<select id="User-RoleId" name="User-RoleId"  disabled>
				<?php
				if (count($model['RoleList']) > 0)
				{
					$i = 1;
					foreach ($model['RoleList'] as $item)
					{
					?>
					<option value="<?php echo $item['Id'];?>" <?php echo ($model['row']['RoleId']  == $item['Id'] ? ' selected' : '');?>>
				<?php echo $item['Code'] . ' ' . $item['Name'];?>
					<?php
					}
				}
				?>
				</select>
			</div>
			<div class="field">
				<label for="User-Email">Email</label>
				<input id="User-Email" name="User-Email" style="width: 80%;" type="email" value="<?php echo $model['row']['Email'];?>"  disabled />
			</div>
			<div class="field">
				<label for="User-PhoneWork">Telefoon werk</label>
				<input id="User-PhoneWork" name="User-PhoneWork" class="text" style="width: 6.25%;" type="text" value="<?php echo $model['row']['PhoneWork'];?>"  disabled />
			</div>
			<div class="field">
				<label for="User-FunctionId">Functie</label>
				<select id="User-FunctionId" name="User-FunctionId"  disabled>
				<?php
				if (count($model['FunctionList']) > 0)
				{
					$i = 1;
					foreach ($model['FunctionList'] as $item)
					{
					?>
					<option value="<?php echo $item['Id'];?>" <?php echo ($model['row']['FunctionId']  == $item['Id'] ? ' selected' : '');?>>
				<?php echo $item['Name'];?>
					<?php
					}
				}
				?>
				</select>
			</div>
			<div class="field">
				<label for="User-OrganisationId">Organisatie</label>
				<select id="User-OrganisationId" name="User-OrganisationId"  disabled>
				<?php
				if (count($model['OrganisationList']) > 0)
				{
					$i = 1;
					foreach ($model['OrganisationList'] as $item)
					{
					?>
					<option value="<?php echo $item['Id'];?>" <?php echo ($model['row']['OrganisationId']  == $item['Id'] ? ' selected' : '');?>>
				<?php echo $item['Name'];?>
					<?php
					}
				}
				?>
				</select>
			</div>
			<div class="field">
				<label for="User-PersonId">Persoon</label>
				<select id="User-PersonId" name="User-PersonId"  disabled>
				<?php
				if (count($model['PersonList']) > 0)
				{
					$i = 1;
					foreach ($model['PersonList'] as $item)
					{
					?>
					<option value="<?php echo $item['Id'];?>" <?php echo ($model['row']['PersonId']  == $item['Id'] ? ' selected' : '');?>>
				<?php echo $item['FirstName'] . ' ' . $item['Lastname'];?>
					<?php
					}
				}
				?>
				</select>
			</div>
			<div class="field">
				<label for="User-CreatedOnDate">Laatst gewijzigd op</label>
				<input id="User-CreatedOnDate" value="<?php echo date('Y-m-d', strtotime($model['row']['CreatedOn']));?>" type="date" name="User-CreatedOnDate"  disabled  />
				<label for="User-CreatedOnTime">om</label>
				<input id="User-CreatedOnTime"  value="<?php echo date('H:i:s', strtotime($model['row']['CreatedOn']));?>" type="time" name="User-CreatedOnTime"  disabled  />
			</div>
			<div class="field">
				<label for="User-UpdatedOnDate">Laatst gewijzigd op</label>
				<input id="User-UpdatedOnDate" value="<?php echo date('Y-m-d', strtotime($model['row']['UpdatedOn']));?>" type="date" name="User-UpdatedOnDate"  disabled  />
				<label for="User-UpdatedOnTime">om</label>
				<input id="User-UpdatedOnTime"  value="<?php echo date('H:i:s', strtotime($model['row']['UpdatedOn']));?>" type="time" name="User-UpdatedOnTime"  disabled  />
			</div>
			<div class="field">
				<input id="User-Id" name="User-Id" style="width: 6em;" type="hidden" value="<?php echo $model['row']['Id'];?>"   />
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</section>
	<?php include('ReadingAll.php'); ?>
</main>
<?php include('Views/Vos/PageFooter.php');?>
