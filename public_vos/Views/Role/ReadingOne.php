<!--  ReadingOne View for Role entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Role/ReadingOne.php/ReadingOne.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
	<section class="detail" id="form" action="/Role/createOne" method="post">
		<header>
			<h2 class="banner">Reading One Role</h2>
			<nav class="command-panel">
				<a href="/Role/UpdatingOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-pencil"></span>
					<span class="screen-reader-text">Updating One</span>
				</a>
				<a href="/Role/CreatingOne" class="tile">
					<span class="icon-plus"></span>
					<span class="screen-reader-text">Creating One</span>
				</a>
				<a href="/Role/DeleteOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-bin"></span>
					<span class="screen-reader-text">Delete One</span>
				</a>
				<a href="/Role/Index" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="Role-Code">Type</label>
				<input id="Role-Code" name="Role-Code" class="text" style="width: 2.5%;" type="text" value="<?php echo $model['row']['Code'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Role-Name">Naam</label>
				<input id="Role-Name" name="Role-Name" class="text" style="width: 20%;" type="text" value="<?php echo $model['row']['Name'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Role-UpdatedOnDate">Laatst gewijzigd op</label>
				<input id="Role-UpdatedOnDate" value="<?php echo date('Y-m-d', strtotime($model['row']['UpdatedOn']));?>" type="date" name="Role-UpdatedOnDate"  disabled  />
				<label for="Role-UpdatedOnTime">om</label>
				<input id="Role-UpdatedOnTime"  value="<?php echo date('H:i:s', strtotime($model['row']['UpdatedOn']));?>" type="time" name="Role-UpdatedOnTime"  disabled  />
			</div>
			<div class="field">
				<input id="Role-Id" name="Role-Id" style="width: 6em;" type="hidden" value="<?php echo $model['row']['Id'];?>"   />
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</section>
	<?php include('ReadingAll.php'); ?>
</main>
<?php include('Views/Vos/PageFooter.php');?>
