<!--  ReadingAll view for Function entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Function/ReadingAll.php/ReadingAll.php
-->
<aside class="list">
	<?php
		if ($model['list'])
		{
	?>
	<table>
		<?php
			foreach ($model['list'] as $item)
			{
		?>
		<tr>
			<td>
				<a class="tile"
				href="/Function/readingOne/<?php echo $item['Id'];?>">
				<span class="icon-arrow-right"></span>
				<span class="screen-reader-text">Select</span></a>
			</td>
			<td>
				<?php echo $item['Name'];?>
			</td>
			<td>
				<?php echo $item['Id'];?>
			</td>

		</tr>
		<?php
		}
		?>
	</table>
	<?php
		}
		else
		{
	?>
	<p>Geen Functie</p>
	<?php
	}
	?>
</aside>

