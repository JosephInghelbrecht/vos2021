<!--  ReadingOne View for Function entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Function/ReadingOne.php/ReadingOne.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
	<section class="detail" id="form" action="/Function/createOne" method="post">
		<header>
			<h2 class="banner">Reading One Function</h2>
			<nav class="command-panel">
				<a href="/Function/UpdatingOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-pencil"></span>
					<span class="screen-reader-text">Updating One</span>
				</a>
				<a href="/Function/CreatingOne" class="tile">
					<span class="icon-plus"></span>
					<span class="screen-reader-text">Creating One</span>
				</a>
				<a href="/Function/DeleteOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-bin"></span>
					<span class="screen-reader-text">Delete One</span>
				</a>
				<a href="/Function/Index" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="Function-Name">Naam</label>
				<input id="Function-Name" name="Function-Name" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Name'];?>"  disabled />
			</div>
			<div class="field">
				<input id="Function-Id" name="Function-Id" style="width: 6em;" type="hidden" value="<?php echo $model['row']['Id'];?>"   />
			</div>
			<div class="field">
				<label for="Function-UpdatedOnDate">Laatst gewijzigd op</label>
				<input id="Function-UpdatedOnDate" value="<?php echo date('Y-m-d', strtotime($model['row']['UpdatedOn']));?>" type="date" name="Function-UpdatedOnDate"    />
				<label for="Function-UpdatedOnTime">om</label>
				<input id="Function-UpdatedOnTime"  value="<?php echo date('H:i:s', strtotime($model['row']['UpdatedOn']));?>" type="time" name="Function-UpdatedOnTime"    />
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</section>
	<?php include('ReadingAll.php'); ?>
</main>
<?php include('Views/Vos/PageFooter.php');?>
