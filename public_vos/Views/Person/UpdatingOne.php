<!--  UpdatingOne View for Person entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Person/UpdatingOne.php/UpdatingOne.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
	<form class="detail" id="form" action="/Person/UpdateOne" method="post">
		<header>
			<h2 class="banner">Updating One Person</h2>
			<nav class="command-panel">
				<button type="submit" value="updateOne" name="updateOne" class="tile">
					<span class="icon-floppy-disk"></span>
					<span class="screen-reader-text">Update One</span>
				</button>
				<a href="/Person/ReadingOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="Person-FirstName">Voornaam</label>
				<input id="Person-FirstName" name="Person-FirstName" class="text" style="width: 12.5%;" type="text" value="<?php echo $model['row']['FirstName'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="Person-LastName">Familienaam</label>
				<input id="Person-LastName" name="Person-LastName" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['LastName'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="Person-Mobile">Gsm</label>
				<input id="Person-Mobile" name="Person-Mobile" class="text" style="width: 6.25%;" type="text" value="<?php echo $model['row']['Mobile'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="Person-Email">Email</label>
				<input id="Person-Email" name="Person-Email" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Email'];?>" required  />
				<span>*</span>
			</div>
			<div class="field">
				<label for="Person-Street">Adres</label>
				<input id="Person-Street" name="Person-Street" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Street'];?>"   />
			</div>
			<div class="field">
				<label for="Person-PostalCode">Postcode</label>
				<input id="Person-PostalCode" name="Person-PostalCode" class="text" style="width: 5%;" type="text" value="<?php echo $model['row']['PostalCode'];?>"   />
			</div>
			<div class="field">
				<label for="Person-City">Woonplaats</label>
				<input id="Person-City" name="Person-City" class="text" style="width: 20%;" type="text" value="<?php echo $model['row']['City'];?>"   />
			</div>
			<div class="field">
				<input id="Person-Id" name="Person-Id" style="width: 6em;" type="hidden" value="<?php echo $model['row']['Id'];?>" required  />
			</div>
			<div class="field">
				<label for="Person-UpdatedOnDate">Laatst gewijzigd op</label>
				<input id="Person-UpdatedOnDate" value="<?php echo date('Y-m-d', strtotime($model['row']['UpdatedOn']));?>" type="date" name="Person-UpdatedOnDate"   required />
				<label for="Person-UpdatedOnTime">om</label>
				<input id="Person-UpdatedOnTime"  value="<?php echo date('H:i:s', strtotime($model['row']['UpdatedOn']));?>" type="time" name="Person-UpdatedOnTime"   required />
					<span>*</span>
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</form>
	<?php include('ReadingAll.php'); ?>
</main>
<?php include('Views/Vos/PageFooter.php');?>
