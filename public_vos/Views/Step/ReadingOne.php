<!--  ReadingOne View for Step entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Step/ReadingOne.php/ReadingOne.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
	<section class="detail" id="form" action="/Step/createOne" method="post">
		<header>
			<h2 class="banner">Reading One Step</h2>
			<nav class="command-panel">
				<a href="/Step/UpdatingOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-pencil"></span>
					<span class="screen-reader-text">Updating One</span>
				</a>
				<a href="/Step/CreatingOne" class="tile">
					<span class="icon-plus"></span>
					<span class="screen-reader-text">Creating One</span>
				</a>
				<a href="/Step/DeleteOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-bin"></span>
					<span class="screen-reader-text">Delete One</span>
				</a>
				<a href="/Step/Index" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="Step-Name">Naam</label>
				<input id="Step-Name" name="Step-Name" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Name'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Step-Description">Omschrijving</label>
				<input id="Step-Description" name="Step-Description" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Description'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Step-ActionId">Actie</label>
				<select id="Step-ActionId" name="Step-ActionId"  disabled>
				<?php
				if (count($model['ActionList']) > 0)
				{
					$i = 1;
					foreach ($model['ActionList'] as $item)
					{
					?>
					<option value="<?php echo $item['Id'];?>" <?php echo ($model['row']['ActionId']  == $item['Id'] ? ' selected' : '');?>>
				<?php echo $item['Code'] . ' ' . $item['Name'];?>
					<?php
					}
				}
				?>
				</select>
			</div>
            <div class="field">
                <label>Actie </label>
                <label><?php echo $model['row']['ActionIdCode'];?></label>
            </div>
			<div class="field">
				<label for="Step-ProcedureId">Procedure</label>
				<select id="Step-ProcedureId" name="Step-ProcedureId"  disabled>
				<?php
				if (count($model['ProcedureList']) > 0)
				{
					$i = 1;
					foreach ($model['ProcedureList'] as $item)
					{
					?>
					<option value="<?php echo $item['Id'];?>" <?php echo ($model['row']['ProcedureId']  == $item['Id'] ? ' selected' : '');?>>
				<?php echo $item['Code'] . ' ' . $item['Name'];?>
					<?php
					}
				}
				?>
				</select>
			</div>
			<div class="field">
				<label for="Step-Order">Volgorde</label>
				<input id="Step-Order" name="Step-Order" style="width: 6em;" type="text" value="<?php echo $model['row']['Order'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Step-Data">Gegevens</label>
				<textarea id="Step-Data" name="Step-Data"  disabled><?php echo $model['row']['Data'];?></textarea>
			</div>
			<div class="field">
				<input id="Step-Id" name="Step-Id" style="width: 6em;" type="hidden" value="<?php echo $model['row']['Id'];?>"   />
			</div>
			<div class="field">
				<label for="Step-UpdatedOnDate">Laatst gewijzigd op</label>
				<input id="Step-UpdatedOnDate" value="<?php echo date('Y-m-d', strtotime($model['row']['UpdatedOn']));?>" type="date" name="Step-UpdatedOnDate"    />
				<label for="Step-UpdatedOnTime">om</label>
				<input id="Step-UpdatedOnTime"  value="<?php echo date('H:i:s', strtotime($model['row']['UpdatedOn']));?>" type="time" name="Step-UpdatedOnTime"    />
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</section>
	<?php include('ReadingAll.php'); ?>
</main>
<?php include('Views/Vos/PageFooter.php');?>
