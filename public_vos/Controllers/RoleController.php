<?php
/* modernways.be
 * created by 3penny
 * Entreprise de modes et de manières modernes
 * Controller for Vos2 app
 * Created on Tuesday 11th of May 2021 04:31:06 PM
 * FileName: Controllers/RoleController.php
*/ 
namespace ModernWays\Controllers;
class RoleController extends \Threepenny\MVC\Controller
{
	public function index()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model['list'] = \Threepenny\CRUD::readAll('Role', 'Name', 'Code,Name');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model);
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}
	public function createOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model = array(
				'tableName' => 'Role',
				'error' => 'Geen'
			);
			$data = array(
				"Code" => filter_input(INPUT_POST, 'Role-Code', FILTER_SANITIZE_STRING),
				"Name" => filter_input(INPUT_POST, 'Role-Name', FILTER_SANITIZE_STRING),
				"UpdatedOn" => filter_input(INPUT_POST, 'Role-UpdatedOnDate', FILTER_SANITIZE_STRING ) . 
					' ' . filter_input(INPUT_POST, 'Role-UpdatedOnTime', FILTER_SANITIZE_STRING )
			);
			$id = \Threepenny\CRUD::create('Role', $data, 'Name', '');
			if ($id > 0) {
				header("Location:/Role/ReadingOne/$id");
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan {$data['Name']} niet toevoegen aan Role";
				$model['error'] = \Threepenny\CRUD::getMessage();
				$model['list'] = \Threepenny\CRUD::readAll('Role', 'Name', 'Code,Name');
				return $this->view($model, 'Views/Role/Index.php');
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function creatingOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model['list'] = \Threepenny\CRUD::readAll('Role', 'Name', 'Code,Name');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model);
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function deleteOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			if (\Threepenny\CRUD::delete('Role', $id, 'Id')) {
				$model['message'] = "Rij gedeleted in Role";
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan rij niet deleten in Role";
				$model['error'] = \Threepenny\CRUD::getMessage();
			}
			$model['list'] = \Threepenny\CRUD::readAll('Role', 'Name', 'Code,Name');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model, 'Views/Role/Index.php');
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function readingOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$row = \Threepenny\CRUD::readOne('Role', $id);
			if ($row) {
				$model['row'] = $row;
				$model['list'] = \Threepenny\CRUD::readAll('Role', 'Name', 'Code,Name');
				$model['message'] = \Threepenny\CRUD::getMessage();
				return $this->view($model);
			}
			else {
				header("Location:/Role/index");
				return false; // just not to get a warning
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function updateOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model = array(
				'tableName' => 'Role',
				'error' => 'Geen'
			);
			$data = array(
				"Code" => filter_input(INPUT_POST, 'Role-Code', FILTER_SANITIZE_STRING),
				"Name" => filter_input(INPUT_POST, 'Role-Name', FILTER_SANITIZE_STRING),
				"UpdatedOn" => filter_input(INPUT_POST, 'Role-UpdatedOnDate', FILTER_SANITIZE_STRING ) . 
					' ' . filter_input(INPUT_POST, 'Role-UpdatedOnTime', FILTER_SANITIZE_STRING ),
				"Id" => filter_input(INPUT_POST, 'Role-Id', FILTER_SANITIZE_NUMBER_INT)
			);
			if (\Threepenny\CRUD::update('Role', $data, 'Name')) {
				$model['message'] = "Rij geüpdated {$data['Name']} in Role";
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan {$data['Name']} niet updaten in Role";
				$model['error'] = \Threepenny\CRUD::getMessage();
			}
			$model['row'] = \Threepenny\CRUD::readOne('Role', $data['Id']);
			$model['list'] = \Threepenny\CRUD::readAll('Role', 'Name', 'Code,Name');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model, 'Views/Role/ReadingOne.php');
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function updatingOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$row = \Threepenny\CRUD::readOne('Role', $id);
			if ($row) {
				$model['row'] = $row;
				$model['list'] = \Threepenny\CRUD::readAll('Role', 'Name', 'Code,Name');
				$model['message'] = \Threepenny\CRUD::getMessage();
				return $this->view($model);
			}
			else {
				header("Location:/Role/index");
				return false; // just not to get a warning
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

}

