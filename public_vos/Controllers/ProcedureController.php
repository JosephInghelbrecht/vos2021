<?php
/* modernways.be
 * created by 3penny
 * Entreprise de modes et de manières modernes
 * Controller for Vos2 app
 * Created on Tuesday 11th of May 2021 04:31:06 PM
 * FileName: Controllers/ProcedureController.php
*/ 
namespace ModernWays\Controllers;
class ProcedureController extends \Threepenny\MVC\Controller
{
	public function index()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model['list'] = \Threepenny\CRUD::readAll('Procedure', 'Name', 'Code,Name,Role.Code,Role.Name');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model);
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}
	public function createOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model = array(
				'tableName' => 'Procedure',
				'error' => 'Geen'
			);
			$data = array(
				"Code" => filter_input(INPUT_POST, 'Procedure-Code', FILTER_SANITIZE_STRING),
				"Name" => filter_input(INPUT_POST, 'Procedure-Name', FILTER_SANITIZE_STRING),
				"Description" => filter_input(INPUT_POST, 'Procedure-Description', FILTER_SANITIZE_STRING),
				"RoleId" => filter_input(INPUT_POST, 'Procedure-RoleId', FILTER_SANITIZE_NUMBER_INT),
				"UpdatedOn" => filter_input(INPUT_POST, 'Procedure-UpdatedOnDate', FILTER_SANITIZE_STRING ) . 
					' ' . filter_input(INPUT_POST, 'Procedure-UpdatedOnTime', FILTER_SANITIZE_STRING )
			);
			$id = \Threepenny\CRUD::create('Procedure', $data, 'Name', '');
			if ($id > 0) {
				header("Location:/Procedure/ReadingOne/$id");
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan {$data['Name']} niet toevoegen aan Procedure";
				$model['error'] = \Threepenny\CRUD::getMessage();
				$model['list'] = \Threepenny\CRUD::readAll('Procedure', 'Name', 'Code,Name,Role.Code,Role.Name');
				return $this->view($model, 'Views/Procedure/Index.php');
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function creatingOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model['list'] = \Threepenny\CRUD::readAll('Procedure', 'Name', 'Code,Name,Role.Code,Role.Name');
			$model['message'] = \Threepenny\CRUD::getMessage();
			$model['RoleList'] = 
				\Threepenny\CRUD::readAll('Role', 'Code,Name', 'Code,Name,Id');
			return $this->view($model);
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function deleteOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			if (\Threepenny\CRUD::delete('Procedure', $id, 'Id')) {
				$model['message'] = "Rij gedeleted in Procedure";
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan rij niet deleten in Procedure";
				$model['error'] = \Threepenny\CRUD::getMessage();
			}
			$model['list'] = \Threepenny\CRUD::readAll('Procedure', 'Name', 'Code,Name,Role.Code,Role.Name');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model, 'Views/Procedure/Index.php');
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function readingOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$row = \Threepenny\CRUD::readOne('Procedure', $id);
			if ($row) {
				$model['row'] = $row;
				$model['list'] = \Threepenny\CRUD::readAll('Procedure', 'Name', 'Code,Name,Role.Code,Role.Name');
                $model['listSteps'] = \Threepenny\CRUD::readAllWhere('Step', $model['row']['Id'],
                    'ProcedureId',  'Name', 'Order,Name,Action.Name');
                var_dump($model['listSteps']);
				$model['message'] = \Threepenny\CRUD::getMessage();
			$model['RoleList'] = 
				\Threepenny\CRUD::readAll('Role', 'Code,Name', 'Code,Name,Id');
				return $this->view($model);
			}
			else {
				header("Location:/Procedure/index");
				return false; // just not to get a warning
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function updateOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model = array(
				'tableName' => 'Procedure',
				'error' => 'Geen'
			);
			$data = array(
				"Code" => filter_input(INPUT_POST, 'Procedure-Code', FILTER_SANITIZE_STRING),
				"Name" => filter_input(INPUT_POST, 'Procedure-Name', FILTER_SANITIZE_STRING),
				"Description" => filter_input(INPUT_POST, 'Procedure-Description', FILTER_SANITIZE_STRING),
				"RoleId" => filter_input(INPUT_POST, 'Procedure-RoleId', FILTER_SANITIZE_NUMBER_INT),
				"UpdatedOn" => filter_input(INPUT_POST, 'Procedure-UpdatedOnDate', FILTER_SANITIZE_STRING ) . 
					' ' . filter_input(INPUT_POST, 'Procedure-UpdatedOnTime', FILTER_SANITIZE_STRING ),
				"Id" => filter_input(INPUT_POST, 'Procedure-Id', FILTER_SANITIZE_NUMBER_INT)
			);
			if (\Threepenny\CRUD::update('Procedure', $data, 'Name')) {
				$model['message'] = "Rij geüpdated {$data['Name']} in Procedure";
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan {$data['Name']} niet updaten in Procedure";
				$model['error'] = \Threepenny\CRUD::getMessage();
			}
			$model['row'] = \Threepenny\CRUD::readOne('Procedure', $data['Id']);
			$model['list'] = \Threepenny\CRUD::readAll('Procedure', 'Name', 'Code,Name,Role.Code,Role.Name');
			$model['message'] = \Threepenny\CRUD::getMessage();
			$model['RoleList'] = 
				\Threepenny\CRUD::readAll('Role', 'Code,Name', 'Code,Name,Id');
			return $this->view($model, 'Views/Procedure/ReadingOne.php');
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function updatingOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$row = \Threepenny\CRUD::readOne('Procedure', $id);
			if ($row) {
				$model['row'] = $row;
				$model['list'] = \Threepenny\CRUD::readAll('Procedure', 'Name', 'Code,Name,Role.Code,Role.Name');
				$model['message'] = \Threepenny\CRUD::getMessage();
		$model['RoleList'] = 
			\Threepenny\CRUD::readAll('Role', 'Code,Name', 'Code,Name,Id');
				return $this->view($model);
			}
			else {
				header("Location:/Procedure/index");
				return false; // just not to get a warning
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

}

