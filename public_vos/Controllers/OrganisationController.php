<?php
/* modernways.be
 * created by 3penny
 * Entreprise de modes et de manières modernes
 * Controller for Vos2 app
 * Created on Tuesday 11th of May 2021 04:31:06 PM
 * FileName: Controllers/OrganisationController.php
*/ 
namespace ModernWays\Controllers;
class OrganisationController extends \Threepenny\MVC\Controller
{
	public function index()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model['list'] = \Threepenny\CRUD::readAll('Organisation', 'Name', 'Name,PostalCode,City');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model);
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}
	public function createOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model = array(
				'tableName' => 'Organisation',
				'error' => 'Geen'
			);
			$data = array(
				"Name" => filter_input(INPUT_POST, 'Organisation-Name', FILTER_SANITIZE_STRING),
				"Street" => filter_input(INPUT_POST, 'Organisation-Street', FILTER_SANITIZE_STRING),
				"PostalCode" => filter_input(INPUT_POST, 'Organisation-PostalCode', FILTER_SANITIZE_STRING),
				"City" => filter_input(INPUT_POST, 'Organisation-City', FILTER_SANITIZE_STRING),
				"Latitude" => filter_input(INPUT_POST, 'Organisation-Latitude', FILTER_SANITIZE_STRING),
				"Longitude" => filter_input(INPUT_POST, 'Organisation-Longitude', FILTER_SANITIZE_STRING),
				"NameManagement" => filter_input(INPUT_POST, 'Organisation-NameManagement', FILTER_SANITIZE_STRING),
				"TelManagement" => filter_input(INPUT_POST, 'Organisation-TelManagement', FILTER_SANITIZE_STRING),
				"UpdatedOn" => filter_input(INPUT_POST, 'Organisation-UpdatedOnDate', FILTER_SANITIZE_STRING ) . 
					' ' . filter_input(INPUT_POST, 'Organisation-UpdatedOnTime', FILTER_SANITIZE_STRING )
			);
			$id = \Threepenny\CRUD::create('Organisation', $data, 'Name', '');
			if ($id > 0) {
				header("Location:/Organisation/ReadingOne/$id");
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan {$data['Name']} niet toevoegen aan Organisation";
				$model['error'] = \Threepenny\CRUD::getMessage();
				$model['list'] = \Threepenny\CRUD::readAll('Organisation', 'Name', 'Name,PostalCode,City');
				return $this->view($model, 'Views/Organisation/Index.php');
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function creatingOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model['list'] = \Threepenny\CRUD::readAll('Organisation', 'Name', 'Name,PostalCode,City');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model);
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function deleteOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			if (\Threepenny\CRUD::delete('Organisation', $id, 'Id')) {
				$model['message'] = "Rij gedeleted in Organisation";
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan rij niet deleten in Organisation";
				$model['error'] = \Threepenny\CRUD::getMessage();
			}
			$model['list'] = \Threepenny\CRUD::readAll('Organisation', 'Name', 'Name,PostalCode,City');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model, 'Views/Organisation/Index.php');
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function readingOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$row = \Threepenny\CRUD::readOne('Organisation', $id);
			if ($row) {
				$model['row'] = $row;
				$model['list'] = \Threepenny\CRUD::readAll('Organisation', 'Name', 'Name,PostalCode,City');
				$model['message'] = \Threepenny\CRUD::getMessage();
				return $this->view($model);
			}
			else {
				header("Location:/Organisation/index");
				return false; // just not to get a warning
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function updateOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model = array(
				'tableName' => 'Organisation',
				'error' => 'Geen'
			);
			$data = array(
				"Name" => filter_input(INPUT_POST, 'Organisation-Name', FILTER_SANITIZE_STRING),
				"Street" => filter_input(INPUT_POST, 'Organisation-Street', FILTER_SANITIZE_STRING),
				"PostalCode" => filter_input(INPUT_POST, 'Organisation-PostalCode', FILTER_SANITIZE_STRING),
				"City" => filter_input(INPUT_POST, 'Organisation-City', FILTER_SANITIZE_STRING),
				"Latitude" => filter_input(INPUT_POST, 'Organisation-Latitude', FILTER_SANITIZE_STRING),
				"Longitude" => filter_input(INPUT_POST, 'Organisation-Longitude', FILTER_SANITIZE_STRING),
				"NameManagement" => filter_input(INPUT_POST, 'Organisation-NameManagement', FILTER_SANITIZE_STRING),
				"TelManagement" => filter_input(INPUT_POST, 'Organisation-TelManagement', FILTER_SANITIZE_STRING),
				"Id" => filter_input(INPUT_POST, 'Organisation-Id', FILTER_SANITIZE_NUMBER_INT),
				"UpdatedOn" => filter_input(INPUT_POST, 'Organisation-UpdatedOnDate', FILTER_SANITIZE_STRING ) . 
					' ' . filter_input(INPUT_POST, 'Organisation-UpdatedOnTime', FILTER_SANITIZE_STRING )
			);
			if (\Threepenny\CRUD::update('Organisation', $data, 'Name')) {
				$model['message'] = "Rij geüpdated {$data['Name']} in Organisation";
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan {$data['Name']} niet updaten in Organisation";
				$model['error'] = \Threepenny\CRUD::getMessage();
			}
			$model['row'] = \Threepenny\CRUD::readOne('Organisation', $data['Id']);
			$model['list'] = \Threepenny\CRUD::readAll('Organisation', 'Name', 'Name,PostalCode,City');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model, 'Views/Organisation/ReadingOne.php');
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function updatingOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$row = \Threepenny\CRUD::readOne('Organisation', $id);
			if ($row) {
				$model['row'] = $row;
				$model['list'] = \Threepenny\CRUD::readAll('Organisation', 'Name', 'Name,PostalCode,City');
				$model['message'] = \Threepenny\CRUD::getMessage();
				return $this->view($model);
			}
			else {
				header("Location:/Organisation/index");
				return false; // just not to get a warning
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

}

