<?php
/* modernways.be
 * created by 3penny
 * Entreprise de modes et de manières modernes
 * Controller for Vos2 app
 * Created on Tuesday 11th of May 2021 04:31:06 PM
 * FileName: Controllers/VosController.php
*/
namespace ModernWays\Controllers;
class VosController extends \Threepenny\MVC\Controller
{
	public function index()
	{
		return $this->view();
	}
	public function login()
	{
		$model['message'] = 'Je moet een geldige gebruikersnaam en paswoord opgeven!';
		if (isset($_POST['User-Name']) && isset($_POST['User-Password'])) {
			$model['message'] = \Threepenny\Identity::login($_POST['User-Name'], $_POST['User-Password']);
		}
		return $this->view($model, 'Views/Vos/Index.php');
	}
	public function logout()
	{
		$model['message'] = \Threepenny\Identity::endSession();
		return $this->view($model, 'Views/Vos/Index.php');
	}
}

