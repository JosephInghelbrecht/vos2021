<?php
/* modernways.be
 * created by 3penny
 * Entreprise de modes et de manières modernes
 * Controller for Vos2 app
 * Created on Tuesday 11th of May 2021 04:31:06 PM
 * FileName: Controllers/UserController.php
*/ 
namespace ModernWays\Controllers;
class UserController extends \Threepenny\MVC\Controller
{
	public function index()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model['list'] = \Threepenny\CRUD::readAll('User', 'Name', 'Name,Role.Code,Role.Name,Function.Name,Organisation.Name,Person.FirstName,Person.Lastname');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model);
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}
	public function createOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model = array(
				'tableName' => 'User',
				'error' => 'Geen'
			);
			$data = array(
				"Name" => filter_input(INPUT_POST, 'User-Name', FILTER_SANITIZE_STRING),
				"Password" => password_hash($_POST['User-Password'], PASSWORD_DEFAULT),
				"RoleId" => filter_input(INPUT_POST, 'User-RoleId', FILTER_SANITIZE_NUMBER_INT),
				"Email" => filter_input(INPUT_POST, 'User-Email', FILTER_SANITIZE_STRING),
				"PhoneWork" => filter_input(INPUT_POST, 'User-PhoneWork', FILTER_SANITIZE_STRING),
				"FunctionId" => filter_input(INPUT_POST, 'User-FunctionId', FILTER_SANITIZE_NUMBER_INT),
				"OrganisationId" => filter_input(INPUT_POST, 'User-OrganisationId', FILTER_SANITIZE_NUMBER_INT),
				"PersonId" => filter_input(INPUT_POST, 'User-PersonId', FILTER_SANITIZE_NUMBER_INT),
				"CreatedOn" => filter_input(INPUT_POST, 'User-CreatedOnDate', FILTER_SANITIZE_STRING ) . 
					' ' . filter_input(INPUT_POST, 'User-CreatedOnTime', FILTER_SANITIZE_STRING ),
				"UpdatedOn" => filter_input(INPUT_POST, 'User-UpdatedOnDate', FILTER_SANITIZE_STRING ) . 
					' ' . filter_input(INPUT_POST, 'User-UpdatedOnTime', FILTER_SANITIZE_STRING )
			);
			$id = \Threepenny\CRUD::create('User', $data, 'Name', 'Password');
			if ($id > 0) {
				header("Location:/User/ReadingOne/$id");
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan {$data['Name']} niet toevoegen aan User";
				$model['error'] = \Threepenny\CRUD::getMessage();
				$model['list'] = \Threepenny\CRUD::readAll('User', 'Name', 'Name,Role.Code,Role.Name,Function.Name,Organisation.Name,Person.FirstName,Person.Lastname');
				return $this->view($model, 'Views/User/Index.php');
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function creatingOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model['list'] = \Threepenny\CRUD::readAll('User', 'Name', 'Name,Role.Code,Role.Name,Function.Name,Organisation.Name,Person.FirstName,Person.Lastname');
			$model['message'] = \Threepenny\CRUD::getMessage();
			$model['RoleList'] = 
				\Threepenny\CRUD::readAll('Role', 'Code,Name', 'Code,Name,Id');
			$model['FunctionList'] = 
				\Threepenny\CRUD::readAll('Function', 'Name', 'Name,Id');
			$model['OrganisationList'] = 
				\Threepenny\CRUD::readAll('Organisation', 'Name', 'Name,Id');
			$model['PersonList'] = 
				\Threepenny\CRUD::readAll('Person', 'FirstName,Lastname', 'FirstName,Lastname,Id');
			return $this->view($model);
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function deleteOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			if (\Threepenny\CRUD::delete('User', $id, 'Id')) {
				$model['message'] = "Rij gedeleted in User";
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan rij niet deleten in User";
				$model['error'] = \Threepenny\CRUD::getMessage();
			}
			$model['list'] = \Threepenny\CRUD::readAll('User', 'Name', 'Name,Role.Code,Role.Name,Function.Name,Organisation.Name,Person.FirstName,Person.Lastname');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model, 'Views/User/Index.php');
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function readingOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$row = \Threepenny\CRUD::readOne('User', $id);
			if ($row) {
				$model['row'] = $row;
				$model['list'] = \Threepenny\CRUD::readAll('User', 'Name', 'Name,Role.Code,Role.Name,Function.Name,Organisation.Name,Person.FirstName,Person.Lastname');
				$model['message'] = \Threepenny\CRUD::getMessage();
			$model['RoleList'] = 
				\Threepenny\CRUD::readAll('Role', 'Code,Name', 'Code,Name,Id');
			$model['FunctionList'] = 
				\Threepenny\CRUD::readAll('Function', 'Name', 'Name,Id');
			$model['OrganisationList'] = 
				\Threepenny\CRUD::readAll('Organisation', 'Name', 'Name,Id');
			$model['PersonList'] = 
				\Threepenny\CRUD::readAll('Person', 'FirstName,Lastname', 'FirstName,Lastname,Id');
				return $this->view($model);
			}
			else {
				header("Location:/User/index");
				return false; // just not to get a warning
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function updateOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model = array(
				'tableName' => 'User',
				'error' => 'Geen'
			);
			$data = array(
				"Name" => filter_input(INPUT_POST, 'User-Name', FILTER_SANITIZE_STRING),
				"RoleId" => filter_input(INPUT_POST, 'User-RoleId', FILTER_SANITIZE_NUMBER_INT),
				"Email" => filter_input(INPUT_POST, 'User-Email', FILTER_SANITIZE_STRING),
				"PhoneWork" => filter_input(INPUT_POST, 'User-PhoneWork', FILTER_SANITIZE_STRING),
				"FunctionId" => filter_input(INPUT_POST, 'User-FunctionId', FILTER_SANITIZE_NUMBER_INT),
				"OrganisationId" => filter_input(INPUT_POST, 'User-OrganisationId', FILTER_SANITIZE_NUMBER_INT),
				"PersonId" => filter_input(INPUT_POST, 'User-PersonId', FILTER_SANITIZE_NUMBER_INT),
				"CreatedOn" => filter_input(INPUT_POST, 'User-CreatedOnDate', FILTER_SANITIZE_STRING ) . 
					' ' . filter_input(INPUT_POST, 'User-CreatedOnTime', FILTER_SANITIZE_STRING ),
				"UpdatedOn" => filter_input(INPUT_POST, 'User-UpdatedOnDate', FILTER_SANITIZE_STRING ) . 
					' ' . filter_input(INPUT_POST, 'User-UpdatedOnTime', FILTER_SANITIZE_STRING ),
				"Id" => filter_input(INPUT_POST, 'User-Id', FILTER_SANITIZE_NUMBER_INT)
			);
			if (\Threepenny\CRUD::update('User', $data, 'Name')) {
				$model['message'] = "Rij geüpdated {$data['Name']} in User";
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan {$data['Name']} niet updaten in User";
				$model['error'] = \Threepenny\CRUD::getMessage();
			}
			$model['row'] = \Threepenny\CRUD::readOne('User', $data['Id']);
			$model['list'] = \Threepenny\CRUD::readAll('User', 'Name', 'Name,Role.Code,Role.Name,Function.Name,Organisation.Name,Person.FirstName,Person.Lastname');
			$model['message'] = \Threepenny\CRUD::getMessage();
			$model['RoleList'] = 
				\Threepenny\CRUD::readAll('Role', 'Code,Name', 'Code,Name,Id');
			$model['FunctionList'] = 
				\Threepenny\CRUD::readAll('Function', 'Name', 'Name,Id');
			$model['OrganisationList'] = 
				\Threepenny\CRUD::readAll('Organisation', 'Name', 'Name,Id');
			$model['PersonList'] = 
				\Threepenny\CRUD::readAll('Person', 'FirstName,Lastname', 'FirstName,Lastname,Id');
			return $this->view($model, 'Views/User/ReadingOne.php');
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function updatingOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$row = \Threepenny\CRUD::readOne('User', $id);
			if ($row) {
				$model['row'] = $row;
				$model['list'] = \Threepenny\CRUD::readAll('User', 'Name', 'Name,Role.Code,Role.Name,Function.Name,Organisation.Name,Person.FirstName,Person.Lastname');
				$model['message'] = \Threepenny\CRUD::getMessage();
		$model['RoleList'] = 
			\Threepenny\CRUD::readAll('Role', 'Code,Name', 'Code,Name,Id');
		$model['FunctionList'] = 
			\Threepenny\CRUD::readAll('Function', 'Name', 'Name,Id');
		$model['OrganisationList'] = 
			\Threepenny\CRUD::readAll('Organisation', 'Name', 'Name,Id');
		$model['PersonList'] = 
			\Threepenny\CRUD::readAll('Person', 'FirstName,Lastname', 'FirstName,Lastname,Id');
				return $this->view($model);
			}
			else {
				header("Location:/User/index");
				return false; // just not to get a warning
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

}

