<?php
// You should create the include directory at the username directory level (that is, one level above the public_html directory).
// This ensures that sensitive files are not in the public_html directory, which anyone can access.
include('../config/vosadmin.php');

include('../vendor/threepenny/mvc/FrontController.php');
include('../vendor/threepenny/mvc/Controller.php');
include('../vendor/threepenny/CRUD.php');
include('../vendor/threepenny/Identity.php');
\Threepenny\Identity::startSession(SESSION_NAME, HTTPS, COOKY_DOMAIN, COOKY_PATH);

include('Controllers/VosController.php');
include('Controllers/OrganisationController.php');
include('Controllers/PersonController.php');
include('Controllers/RoleController.php');
include('Controllers/FunctionController.php');

include('Controllers/UserController.php');
include('Controllers/LogController.php');
include('Controllers/ProcedureController.php');
include('Controllers/StepController.php');
include('Controllers/ActionController.php');

// default namespace is root \, otherwise specify it as argument
// default controller name is , otherwise specify it as argument
// default action method name is , otherwise specify it as argument
$route = \Threepenny\MVC\FrontController::getRouteData($_SERVER['REQUEST_URI'], 'ModernWays', 'Vos', 'index');
$view = \Threepenny\MVC\FrontController::Dispatch($route);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vos beheer</title>
</head>
<body>
<?php $view(); ?>
</body>
</html>