<?php
// include('../config/vosadmin.php');
// include('../vendor/threepenny/CRUD.php');
$json = file_get_contents('organisationList.json');
$data = json_decode($json, TRUE);
echo '<pre>';
//var_dump($data);
echo '</pre>';
foreach($data as $item) {
    $row = array(
        "Name" => $item['name'],
        "Street" => $item['street'],
        "PostalCode" => $item['postalcode'],
        "City" => $item['city'],
        "Latitude" => $item['latitude'],
        "Longitude" => $item['longitude'],
        "NameManagement" => $item['directie']['firstName'] . ' ' . $item['directie']['lastName'],
        "TelManagement" => $item['phone'],
        "UpdatedOn" => date('Y-m-d H:i:s'));
    if (\Threepenny\CRUD::create('Organisation', $row, 'Name')) {
        echo "Rij toegevoegd! {$row['Name']} is toegevoegd aan Curiosity";
    } else {
        echo "Oeps er is iets fout gelopen! Kan {$row['Name']} niet toevoegen aan Curiosity";
        echo \Threepenny\CRUD::getMessage();
    }
}
