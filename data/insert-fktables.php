<?php
include('../config/vosadmin.php');
include('../vendor/threepenny/CRUD.php');

include ('json2sql-org.php');

$data = array(
    ['Code' => 'ALARM', 'Name' => 'Geef alarm via tel, sms en notificatie', 'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Code' => 'YESNO', 'Name' => 'Ja of nee', 'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Code' => 'LIST', 'Name' => 'List', 'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Code' => 'TEXT', 'Name' => 'Tekst instructie', 'UpdatedOn' => date('Y-m-d H:i:s')]

);

$id = \Threepenny\CRUD::create('Action', $data, 'Name');
echo \Threepenny\CRUD::getMessage() . '<br/>';

$data = array(
    ['Name' => 'Directeur', 'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Name' => 'Adjunct directeur', 'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Name' => 'ICT', 'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Name' => 'Secretariaat', 'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Name' => 'Docent', 'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Name' => 'Leerkracht', 'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Name' => 'Preventie adviseur', 'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Name' => 'Lector', 'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Name' => 'Stafmedewerker', 'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Name' => 'Boekhouder', 'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Name' => 'Leerling', 'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Name' => 'Student', 'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Name' => 'Onderhoud', 'UpdatedOn' => date('Y-m-d H:i:s')]
);

$id = \Threepenny\CRUD::create('Function', $data, 'Name');
echo \Threepenny\CRUD::getMessage() . '<br/>';

$role = array(
    ['Code' => 'Admin',
        'Name' => 'Beheerder',
        'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Code' => 'DV',
        'Name' => 'Direct verantwoordelijke',
        'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Code' => 'LDO',
        'Name' => 'Leerkracht, docent, ...',
        'UpdatedOn' => date('Y-m-d H:i:s')],
    ['Code' => 'GAST',
        'Name' => 'Gast, student, leerling',
        'UpdatedOn' => date('Y-m-d H:i:s')]
);

$id = \Threepenny\CRUD::create('Role', $role, 'Name');
echo \Threepenny\CRUD::getMessage() . '<br/>';

$person = array(
    ['FirstName' => 'Jef',
        'LastName' => 'Inghelbrecht',
        'UpdatedOn' => date('Y-m-d H:i:s'),
        'Email' => 'jef.inghelbrecht@outlook.be'],
    ['FirstName' => 'David',
        'LastName' => 'Op de Kamp',
        'UpdatedOn' => date('Y-m-d H:i:s'),
        'Email' => 'david.opdekamp@ap.be'],
    ['FirstName' => 'Jan',
        'LastName' => 'Robberecht',
        'UpdatedOn' => date('Y-m-d H:i:s'),
        'Email' => 'jan.robberecht@ap.be'],
    ['FirstName' => 'Seppe',
        'LastName' => 'Van Broeckhoven',
        'UpdatedOn' => date('Y-m-d H:i:s'),
        'Email' => 'seppe.vanbroeckhoven@ap.be']
);

$id = \Threepenny\CRUD::create('Person', $person, 'LastName', array('LastName'));
echo \Threepenny\CRUD::getMessage() . '<br/>';

$user = array(
    ['Name' => 'Admin Jef',
        'Email' => 'joseph.inghelbrecht@ap.be',
        'OrganisationId' => 1,
        'FunctionId' => 3,
        'PersonId' => 1,
        'Password' => password_hash('Dollar1Baby', PASSWORD_DEFAULT),
        'CreatedOn' => date('Y-m-d H:i:s'),
        'UpdatedOn' => date('Y-m-d H:i:s'),
        'RoleId' => '1'],
    ['Name' => 'Admin David',
        'Email' => 'david.opdekamp@ap.be',
        'OrganisationId' => 1,
        'FunctionId' => 3,
        'PersonId' => 2,
        'CreatedOn' => date('Y-m-d H:i:s'),
        'UpdatedOn' => date('Y-m-d H:i:s'),
        'Password' => password_hash('Dollar2Baby', PASSWORD_DEFAULT),
        'RoleId' => '1'],
    ['Name' => 'Admin Jan',
        'Email' => 'jan.robberecht@ap.be',
        'OrganisationId' => 1,
        'FunctionId' => 3,
        'PersonId' => 3,
        'Password' => password_hash('Dollar3Baby', PASSWORD_DEFAULT),
        'CreatedOn' => date('Y-m-d H:i:s'),
        'UpdatedOn' => date('Y-m-d H:i:s'),
        'RoleId' => '1'],
    ['Name' => 'Admin Seppe',
        'Email' => 'seppe.vanbroeckhoven@ap.be',
        'OrganisationId' => 1,
        'FunctionId' => 3,
        'PersonId' => 4,
        'Password' => password_hash('Dollar4Baby', PASSWORD_DEFAULT),
        'CreatedOn' => date('Y-m-d H:i:s'),
        'UpdatedOn' => date('Y-m-d H:i:s'),
        'RoleId' => '1']
);

$id = \Threepenny\CRUD::create('User', $user, 'Name', array('Password'));
echo \Threepenny\CRUD::getMessage() . '<br/>';

include ('json2sql-proc.php');