-- 3penny -- Tuesday 11th of May 2021 12:22:02 PM
--
SET
GLOBAL TRANSACTION ISOLATION LEVEL SERIALIZABLE;
-- mode changes syntax and behavior to conform more closely to standard SQL.
-- It is one of the special combination modes listed at the end of this section.
SET
GLOBAL sql_mode = 'ANSI';
-- If database does not exist, create the database
CREATE
DATABASE IF NOT EXISTS Vos2;
USE
`Vos2`;
-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET
FOREIGN_KEY_CHECKS = 0;

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Organisation
-- Created on Tuesday 11th of May 2021 12:22:02 PM
--
DROP TABLE IF EXISTS `Organisation`;
CREATE TABLE `Organisation`
(
    `Name`           NVARCHAR (255) NOT NULL,
    `Street`         NVARCHAR (255) NOT NULL,
    `PostalCode`     VARCHAR(20)   NOT NULL,
    `City`           NVARCHAR (80) NOT NULL,
    `Latitude`       DECIMAL(7, 5) NOT NULL,
    `Longitude`      DECIMAL(8, 5) NOT NULL,
    `NameManagement` NVARCHAR (80) NOT NULL,
    `TelManagement`  VARCHAR(25)   NOT NULL,
    `Id`             INT           NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    `UpdatedOn`      TIMESTAMP     NOT NULL,
    CONSTRAINT uc_Organisation_Name UNIQUE (Name)
);

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE User
-- Created on Tuesday 11th of May 2021 12:22:02 PM
--
DROP TABLE IF EXISTS `User`;
CREATE TABLE `User`
(
    `Name`           NVARCHAR (50) NOT NULL,
    `Password`       NVARCHAR (255) NULL,
    `RoleId`         INT       NOT NULL,
    `Email`          NVARCHAR (255) NOT NULL,
    `PhoneWork`      VARCHAR(25) NULL,
    `FunctionId`     INT       NOT NULL,
    `OrganisationId` INT NULL,
    `PersonId`       INT       NOT NULL,
    `CreatedOn`      TIMESTAMP NOT NULL,
    `UpdatedOn`      TIMESTAMP NOT NULL,
    `Id`             INT       NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    CONSTRAINT uc_User_Name UNIQUE (Name),
    CONSTRAINT fk_UserRoleId FOREIGN KEY (`RoleId`) REFERENCES `Role` (`Id`),
    CONSTRAINT fk_UserFunctionId FOREIGN KEY (`FunctionId`) REFERENCES `Function` (`Id`),
    CONSTRAINT fk_UserOrganisationId FOREIGN KEY (`OrganisationId`) REFERENCES `Organisation` (`Id`),
    CONSTRAINT fk_UserPersonId FOREIGN KEY (`PersonId`) REFERENCES `Person` (`Id`)
);

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Function
-- Created on Tuesday 11th of May 2021 12:22:02 PM
--
DROP TABLE IF EXISTS `Function`;
CREATE TABLE `Function`
(
    `Name`      NVARCHAR (89) NOT NULL,
    `Id`        INT       NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    `UpdatedOn` TIMESTAMP NOT NULL,
    CONSTRAINT uc_Function_Name UNIQUE (Name)
);

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Person
-- Created on Tuesday 11th of May 2021 12:22:02 PM
--
DROP TABLE IF EXISTS `Person`;
CREATE TABLE `Person`
(
    `FirstName`  NVARCHAR (50) NOT NULL,
    `LastName`   NVARCHAR (100) NOT NULL,
    `Mobile`     VARCHAR(25) NOT NULL,
    `Email`      NVARCHAR (255) NOT NULL,
    `Street`     NVARCHAR (255) NULL,
    `PostalCode` VARCHAR(20) NULL,
    `City`       NVARCHAR (80) NULL,
    `Id`         INT         NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    `UpdatedOn`  TIMESTAMP   NOT NULL,
    CONSTRAINT uc_Person_Email UNIQUE (Email)
);

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Procedure
-- Created on Tuesday 11th of May 2021 12:22:02 PM
--
DROP TABLE IF EXISTS `Procedure`;
CREATE TABLE `Procedure`
(
    `Code`        VARCHAR(50) NOT NULL,
    `Name`        NVARCHAR (255) NOT NULL,
    `Description` NVARCHAR (255) NULL,
    `RoleId`      INT         NOT NULL,
    `UpdatedOn`   TIMESTAMP   NOT NULL,
    `Id`          INT         NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    CONSTRAINT fk_ProcedureRoleId FOREIGN KEY (`RoleId`) REFERENCES `Role` (`Id`)
);

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Role
-- Created on Tuesday 11th of May 2021 12:22:02 PM
--
DROP TABLE IF EXISTS `Role`;
CREATE TABLE `Role`
(
    `Code`      NVARCHAR (10) NOT NULL,
    `Name`      NVARCHAR (80) NOT NULL,
    `UpdatedOn` TIMESTAMP NOT NULL,
    `Id`        INT       NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    CONSTRAINT uc_Role_Code UNIQUE (Code)
);

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Step
-- Created on Tuesday 11th of May 2021 12:22:02 PM
--
DROP TABLE IF EXISTS `Step`;
CREATE TABLE `Step`
(
    `Name`        NVARCHAR (255) NOT NULL,
    `Description` NVARCHAR (255) NULL,
    `ActionId`    INT       NOT NULL,
    `ProcedureId` INT       NOT NULL,
    `Order`       INT NULL,
    `Data`        NVARCHAR (4096) NOT NULL,
    `Id`          INT       NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    `UpdatedOn`   TIMESTAMP NOT NULL,
    CONSTRAINT fk_StepActionId FOREIGN KEY (`ActionId`) REFERENCES `Action` (`Id`),
    CONSTRAINT fk_StepProcedureId FOREIGN KEY (`ProcedureId`) REFERENCES `Procedure` (`Id`)
);

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Action
-- Created on Tuesday 11th of May 2021 12:22:02 PM
--
DROP TABLE IF EXISTS `Action`;
CREATE TABLE `Action`
(
    `Code`      NVARCHAR (10) NOT NULL,
    `Name`      NVARCHAR (80) NOT NULL,
    `UpdatedOn` TIMESTAMP NOT NULL,
    `Id`        INT       NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    CONSTRAINT uc_Action_Code UNIQUE (Code)
);

-- modernways.be
-- created by 3penny
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Log
-- Created on Tuesday 11th of May 2021 12:22:02 PM
--
DROP TABLE IF EXISTS `Log`;
CREATE TABLE `Log`
(
    `UserName`       NVARCHAR (50) NOT NULL,
    `Email`          NVARCHAR (255) NOT NULL,
    `Role`           NVARCHAR (50) NOT NULL,
    `ProcedureCode`  NVARCHAR (25) NOT NULL,
    `ProcedureTitle` NVARCHAR (255) NOT NULL,
    `StepTitle`      VARCHAR(255) NOT NULL,
    `ActionCode`     NVARCHAR (10) NOT NULL,
    `CallNumber`     VARCHAR(25)  NOT NULL,
    `SendNumber`     VARCHAR(25)  NOT NULL,
    `Id`             INT          NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY (`Id`),
    `UpdatedOn`      TIMESTAMP    NOT NULL
);

-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET
FOREIGN_KEY_CHECKS = 1;

